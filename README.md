# TO DO

## Descripción de la aplicación

En la primera versión de la aplicación queremos una aplicación que se ejecute por comandos de consola.

Los comandos a ejecutar:

- node app crear
  -d "Descripción de la tarea"
  --descripcion "Descripción de la tarea"
- node app actualizar
  -d "Descripción de la tarea"
  --descripcion "Descripción de la tarea"
  -c true -> true si la tarea está completada, false en caso contrario, este valor por defecto será true
  --completada true
- node app eliminar
  -d "Descripción de la tarea"
  --descripcion "Descripción de la tarea"
- node app listar: lista todas las tareas
- node app listar
  -d "Descripción de la tarea"
  --descripcion "Descripción de la tarea"
- node app listar: listar todas las tareas
  -c true: listar tareas completas
  -c false: listar tareas pendientes

## Tecnologías utilizadas

## Tareas pendientes

- Mejorar funcionalidad:
  - Si intentamos actualzar una tarea que no se encuentra en la base de datos devolver error
  - Si intentamos eliminar una tarea que no se encuentra en la base de datos devolver error
  - Mostrar el listada que contengan alguna descripción.

## Dudas

- Manejo de excepciones
