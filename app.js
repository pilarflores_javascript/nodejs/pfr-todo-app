const { argv } = require("./config/config");
const {
    crearTarea,
    actualizarTarea,
    eliminarTarea,
    listarTarea,
} = require("./services/tareaService");
const colors = require("colors");

const command = argv._[0];

switch (command) {
    case "crear":
        console.log(
            `[APP] Se inicia la petición de 'Crear Nueva Tarea', ${argv.descripcion}`
            .cyan
        );
        crearTarea(argv.descripcion)
            .then((tarea) => {
                console.log(`[APP] Tarea ${tarea.descripcion} creada con éxito.`.green);
            })
            .catch(console.log);
        break;

    case "actualizar":
        console.log(`[APP] Se inicia la petición de 'Actualizar Tarea'.`.cyan);
        actualizarTarea(argv.descripcion, argv.completada);
        console.log(`[APP] Tarea ${argv.descripcion} actualizada con éxito.`.green);
        break;

    case "eliminar":
        console.log(
            `[APP] Se inicia la petición de 'Eliminar Tarea',  ${argv.descripcion}.`
            .cyan
        );
        eliminarTarea(argv.descripcion);
        break;

    case "listar":
        listarTarea(argv.completada)
            .then((listadoTareas) => {
                console.log("#####################################".cyan);
                console.log("############   TAREAS   #############".cyan);
                console.log("#####################################".cyan);

                for (let tarea of listadoTareas) {
                    console.log("=============== Tarea =============".green);
                    console.log(tarea.descripcion);
                    console.log("Estado: ", tarea.completada);
                    console.log("====================================".green);
                }
            })
            .catch((error) => console.error(error));

        break;

    default:
        console.error(
            `[ERROR] No existe lógica para el comando introducido: ${command}`.red
        );

        break;
}