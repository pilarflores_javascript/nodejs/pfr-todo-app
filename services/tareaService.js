const { save, get } = require("../repositories/tareasRepository");

let listaTareas = [];

let crearTarea = async(descripcion) => {
    let tarea = {
        descripcion,
        completada: "false",
    };

    listaTareas = await get();

    if (listaTareas.find((t) => t.descripcion === descripcion))
        throw new Error(
            `La tarea ${descripcion} ya existe. Cambie la descripción. `
        );

    listaTareas.push(tarea);

    save(listaTareas);

    return tarea;
};

let actualizarTarea = async(descripcion, completada) => {
    console.log(
        `Se actualiza la tarea ${descripcion} con el estado de ${completada}`
    );

    listaTareas = await get();

    listaTareas
        .filter((t) => t.descripcion === descripcion)
        .map((t) => (t.completada = completada));

    save(listaTareas);
};

let eliminarTarea = async(descripcion) => {
    console.log(`Se elimina la tarea ${descripcion}.`);
    listaTareas = await get();

    listaTareasActualizada = listaTareas.filter(
        (t) => t.descripcion !== descripcion
    );

    save(listaTareasActualizada);
};

let listarTarea = async(completada) => {
    listaTareas = await get();

    if (Array.isArray(listaTareas) && listaTareas.length) {
        if (!completada) {
            return listaTareas;
        } else {
            let listaFiltrada = listaTareas.filter(
                (t) => t.completada === completada
            );
            return listaFiltrada;
        }
    }

    throw new Error(`No hay tareas creadas.`);
};

module.exports = {
    crearTarea,
    actualizarTarea,
    eliminarTarea,
    listarTarea,
};