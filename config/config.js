const descripcion = {
    demand: true,
    alias: "d",
    desc: "Descripción de la tarea",
};

const completada = {
    demand: false,
    alias: "c",
    desc: "Indicador estado tarea, informar como 'true' en caso de completada",
    default: "true",
};

const argv = require("yargs")
    .command("crear", "Crear tarea", {
        descripcion,
    })
    .command("actualizar", "Actualizar tarea", {
        descripcion,
        completada,
    })
    .command("eliminar", "Eliminar tarea", {
        descripcion,
    })
    .command("listar", "Listar tareas", {
        descripcion: {
            demand: false,
            alias: "d",
            desc: "Descripción de la tarea",
        },
        completada: {
            demand: false,
            alias: "c",
            desc: "Informar como 'true' en caso de querer obtener las tareas completadas, o false en caso de querer obtener las tareas no completadas",
        },
    })
    .help().argv;

module.exports = {
    argv,
};