const fs = require("fs");

let listaTareas = [];

let save = (listaTareas) => {
    let dataDB = JSON.stringify(listaTareas);

    fs.writeFile("./repositories/tareasDB.json", dataDB, (err) => {
        if (err) throw err;
    });
};

let get = async() => {
    try {
        listaTareas = require("./tareasDB.json");
        return listaTareas;
    } catch (error) {
        return [];
    }
};

module.exports = {
    save,
    get,
};